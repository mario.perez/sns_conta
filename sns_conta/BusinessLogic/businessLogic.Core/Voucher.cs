﻿using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;
using System;
using System.Collections.Generic;
using System.Data;

namespace businessLogic.Core
{
    public class Voucher : CustomCatalogManager<Voucher>, ICRUD<Voucher>
    {
        #region Private Declarations...
        private decimal _voucherId;
        private DateTime _voucherDate;
        private string _creationDate;
        private string _concept;
        private decimal _debit;
        private decimal _credit;
        private bool _isEquivalent;
        private string _comments;
        private string _voucherTypeId;
        private bool _isActive;
        private string _beneficiary;
        private VoucherDetail[] _details;
        
        #endregion

        #region Public Attribute...
        public decimal VoucherId { get => _voucherId; set => _voucherId = value; }
        public DateTime VoucherDate { get => _voucherDate; }
        public string CreationDate { get => _creationDate; }
        public string Concept { get => _concept; }
        public decimal Debit { get => _debit; }
        public decimal Credit { get => _credit; }
        public bool IsEquivalent { get => _isEquivalent; }
        public string Comments { get => _comments; }
        public string VoucherTypeId { get => _voucherTypeId; }
        public bool IsActive { get => _isActive;}
        public VoucherDetail[] Details { get => _details; }
        public string Beneficiary { get => _beneficiary; set => _beneficiary = value; }


        #endregion
        public Voucher()
        {
            _procedureName = "sp_comprobante";
        }

        public Voucher(decimal voucherId, DateTime voucherDate, string creationDate,
                    string concept, decimal debit, decimal credit,
                    bool isEquivalent, string comments, string voucherTypeId, 
                    bool isActive, VoucherDetail[] details, string beneficiary)
        {
            _procedureName = "sp_comprobante";
            _voucherId = voucherId;
            _voucherDate = voucherDate;
            _creationDate = creationDate;
            _concept = concept;
            _debit = debit;
            _credit = credit;
            _isEquivalent = isEquivalent;
            _comments = comments;
            _voucherTypeId = voucherTypeId;
            _isActive = isActive;
            _details = details;
            _beneficiary = beneficiary;
        }

        protected override void setCatalog(DataAccess dataAccess, Voucher data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.Numeric, data.VoucherId);
            dataAccess.AdParameter("@i_fechaComprobante", DBTypeEnum.VarChar, data.VoucherDate);
            dataAccess.AdParameter("@i_concepto", DBTypeEnum.VarChar, data.Concept);
            dataAccess.AdParameter("@i_debe", DBTypeEnum.Decimal, data.Debit);
            dataAccess.AdParameter("@i_haber", DBTypeEnum.Decimal, data.Credit);
            dataAccess.AdParameter("@i_esEquivalente", DBTypeEnum.VarChar, data.IsEquivalent ? "S" : "N");
            dataAccess.AdParameter("@i_observaciones", DBTypeEnum.VarChar, data.Comments);
            dataAccess.AdParameter("@i_idTipoComprobante", DBTypeEnum.VarChar, data.VoucherTypeId);
            dataAccess.AdParameter("@i_es_activo", DBTypeEnum.VarChar, data.IsActive ? "S" : "N");
            dataAccess.AdParameter("@i_beneficiario", DBTypeEnum.VarChar, data.Beneficiary);
            dataAccess.AdParameterOutput("@o_codigo", DBTypeEnum.Numeric, null);
        }

        protected override Voucher getNewCatalog()
        {
            return new Voucher();
        }

        protected override void setOutputs(out Dictionary<string, string> parameters)
        {
            parameters = new Dictionary<string, string>();
            parameters.Add("@o_codigo", "");
        }

        protected override Voucher getCatalog(DataRow row)
        {
            var strVoucherDate = row[1].ToString();
            var strCreationDate = row[2].ToString();
            var tmpVoucherDate = new DateTime(int.Parse(strVoucherDate.Substring(6, 4)), int.Parse(strVoucherDate.Substring(3, 2)), int.Parse(strVoucherDate.Substring(0, 2)));
            

            var result = new Voucher(decimal.Parse(row[0].ToString()), tmpVoucherDate, strCreationDate, row[3].ToString(),
                                    decimal.Parse(row[4].ToString()), decimal.Parse(row[5].ToString()),
                                    row[6].ToString() == "S", row[7].ToString(), row[8].ToString(),
                                    row[9].ToString() == "S", new List<VoucherDetail>().ToArray(),
                                    row[10].ToString());
            return result;
        }

        public bool SaveOneShot(Voucher data, out string OutputCode)
        {
            var result = Add(data);
            data.VoucherId = decimal.Parse(_parameters["@o_codigo"]);
            OutputCode = data.VoucherId.ToString();
            foreach(var item in data.Details)
            {
                item.VoucherId = data.VoucherId;
            }
            var resultDetails = new VoucherDetail().LoadBulk(data.Details);
            return result && resultDetails;
        }

        public List<Voucher> GetFiltered(string voucherTypeId, decimal voucherId, DateTime initialDate, DateTime finalDate, string concept, string beneficiary)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            var myConcept = string.IsNullOrWhiteSpace(concept)?string.Empty:concept.Replace("*", "%");
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "F");
            //dataAccess.AdParameter("@i_user", DBTypeEnum.VarChar, "");
            //dataAccess.AdParameter("@i_terminal", DBTypeEnum.VarChar, "");
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.Numeric, voucherId);
            dataAccess.AdParameter("@i_fechaComprobante", DBTypeEnum.VarChar, initialDate.ToString("yyyyMMdd"));
            dataAccess.AdParameter("@i_fechaComprobanteFin", DBTypeEnum.VarChar, finalDate.ToString("yyyyMMdd"));
            dataAccess.AdParameter("@i_concepto", DBTypeEnum.VarChar, myConcept);
            dataAccess.AdParameter("@i_idTipoComprobante", DBTypeEnum.VarChar, voucherTypeId);
            dataAccess.AdParameter("@i_beneficiario", DBTypeEnum.VarChar, beneficiary);

            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                }
                else
                {
                    response = new DataTable();
                }
            }

            List<Voucher> catalogList = new List<Voucher>();
            if (response.Rows == null)
            {
                return catalogList;
            }
            foreach (DataRow row in response.Rows)
            {
                Voucher catalog = getCatalog(row);
                catalogList.Add(catalog);
            }
            return catalogList;


        }

    }
}
