﻿using System.Data;
using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;

namespace businessLogic.Core
{
    public class EntityPerson:CustomCatalogManager<EntityPerson>,ICRUD<EntityPerson>
    {
        #region Private Declarations...
        private int _id;
        private string _name;
        private string _entityType;
        private string _document;
        private string _documentType;
        private string _address;
        private string _telephone;
        private bool _isActive;
        #endregion

        #region Public Attribute...
        public int Id { get => _id;  }
        public string Name { get => _name; }
        public bool IsActive { get => _isActive;  }
        public string Document { get => _document;  }
        public string EntityType { get => _entityType; }
        public string DocumentType { get => _documentType;}
        public string Address { get => _address; }
        public string Telephone { get => _telephone;}
        #endregion

        public EntityPerson()
        {
            _procedureName = "sp_entidad";
        }

        public EntityPerson(int id, string name, string document, string entityType, string documentType, string address, string telephone ,bool isActive)
        {
            _procedureName = "sp_entidad";
            _id = id;
            _name = name;
            _document = document;
            _entityType = entityType;
            _documentType = documentType;
            _address = address;
            _telephone = telephone;
            _isActive = isActive;
        }


        protected override void setCatalog(DataAccess dataAccess, EntityPerson data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.Integer, data.Id);
            dataAccess.AdParameter("@i_nombre", DBTypeEnum.VarChar, data.Name);
            dataAccess.AdParameter("@i_tipoEntidad", DBTypeEnum.VarChar, data.EntityType);
            dataAccess.AdParameter("@i_documento", DBTypeEnum.VarChar, data.Document);
            dataAccess.AdParameter("@i_tipoDocumento", DBTypeEnum.Integer, data.DocumentType);
            dataAccess.AdParameter("@i_direccion", DBTypeEnum.VarChar, data.Address);
            dataAccess.AdParameter("@i_telefono", DBTypeEnum.VarChar, data.Telephone);
            dataAccess.AdParameter("@i_es_activo", DBTypeEnum.VarChar, data.IsActive?"S":"N");
        }

        protected override EntityPerson getNewCatalog()
        {
            return new EntityPerson();
        }

        protected override EntityPerson getCatalog(DataRow row)
        {
            var result = new EntityPerson(int.Parse(row[0].ToString()), row[1].ToString(), row[3].ToString(), row[2].ToString(),
                                            row[4].ToString(), row[5].ToString(), row[6].ToString(), row[7].ToString()=="S");
            return result;
        }


    }
}
