﻿using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Core
{
    public class VoucherType: CustomCatalogManager<VoucherType>, ICRUD<VoucherType>
    {
        #region Private Declarations...
        private string _voucherTypeId;
        private string _voucherTypeName;
        private bool _isActive;
        #endregion

        #region Public Attribute...
        public string VoucherTypeId { get => _voucherTypeId;}
        public string VoucherTypeName { get => _voucherTypeName;}
        public bool IsActive { get => _isActive;}
        #endregion

        public VoucherType()
        {
            _procedureName = "sp_tipoComprobante";
        }

        public VoucherType(string voucherTypeId, string voucherTypeName, bool isActive)
        {
            _procedureName = "sp_tipoComprobante";
            _voucherTypeId = voucherTypeId;
            _voucherTypeName = voucherTypeName;
            _isActive = isActive;
        }

        protected override void setCatalog(DataAccess dataAccess, VoucherType data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, data.VoucherTypeId);
            dataAccess.AdParameter("@i_nombre", DBTypeEnum.VarChar, data.VoucherTypeName);            
            dataAccess.AdParameter("@i_es_activo", DBTypeEnum.VarChar, data.IsActive ? "S" : "N");
        }

        protected override VoucherType getNewCatalog()
        {
            return new VoucherType();
        }

        protected override VoucherType getCatalog(DataRow row)
        {
            var result = new VoucherType(row[0].ToString(),row[1].ToString(), row[2].ToString() == "S");
            return result;
        }



    }
}
