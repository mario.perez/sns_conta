﻿using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Core
{
    public class AccountingCatalog : CustomCatalogManager<AccountingCatalog>, ICRUD<AccountingCatalog>
    {
        #region Private Declarations...
        private decimal _accountId;
        private string _accountName;
        private string _accountingChain;
        private int _level;
        private decimal _parentAccountId;
        private bool _isActive;
        #endregion

        #region Public Attribute...        
        public bool IsActive { get => _isActive;  }
        public decimal AccountId { get => _accountId;}
        public string AccountName { get => _accountName;}
        public string AccountingChain { get => _accountingChain; }
        public int Level { get => _level; }
        public decimal ParentAccountId { get => _parentAccountId;}
        #endregion

        public AccountingCatalog()
        {
            _procedureName = "sp_catalogoContable";
        }

        public AccountingCatalog(decimal accountId, string accountName, string accountingChain,
                        int level, decimal parentAccountId, bool isActive)
        {
            _procedureName = "sp_catalogoContable";
            _accountId = accountId;
            _accountName = accountName;
            _accountingChain = accountingChain;
            _level = level;
            _parentAccountId = parentAccountId;
            _isActive = isActive;
        }

        protected override void setCatalog(DataAccess dataAccess, AccountingCatalog data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.Numeric, data.AccountId);
            dataAccess.AdParameter("@i_nombre", DBTypeEnum.VarChar, data.AccountName);
            dataAccess.AdParameter("@i_cadenaContable", DBTypeEnum.VarChar, data.AccountingChain);
            dataAccess.AdParameter("@i_nivel", DBTypeEnum.Integer, data.Level);
            dataAccess.AdParameter("@i_idCuentaPadre", DBTypeEnum.Numeric, data.ParentAccountId);
            dataAccess.AdParameter("@i_es_activo", DBTypeEnum.VarChar, data.IsActive ? "S" : "N");
        }
        protected override AccountingCatalog getCatalog(DataRow row)
        {
            var result = new AccountingCatalog(decimal.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString(),
                                int.Parse(row[3].ToString()),decimal.Parse(row[4].ToString()), row[5].ToString() == "S");
            return result;
        }

        protected override AccountingCatalog getNewCatalog()
        {
            return new AccountingCatalog();
        }

    }
}
