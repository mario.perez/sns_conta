﻿using crossCutting.Resources;
using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Core
{
    public class VoucherDetail: CustomCatalogManager<VoucherDetail>, ICRUD<VoucherDetail>
    {
        #region Private Declarations...
        private decimal _voucherId;
        private decimal _voucherDetailId;
        private string _concept;
        private decimal _debit;
        private decimal _credit;
        private decimal _accountId;
        private bool _isActive;
        #endregion

        #region Public Attribute...
        public decimal VoucherId { get => _voucherId; set => _voucherId = value; }
        public decimal VoucherDetailId { get => _voucherDetailId; set => _voucherDetailId = value; }
        public string Concept { get => _concept; set => _concept = value; }
        public decimal Debit { get => _debit; set => _debit = value; }
        public decimal Credit { get => _credit; set => _credit = value; }
        public decimal AccountId { get => _accountId; set => _accountId = value; }
        public bool IsActive { get => _isActive; set => _isActive = value; }
        #endregion

        public VoucherDetail()
        {
            _procedureName = "sp_detalleComprobante";
        }

        public VoucherDetail(decimal voucherId, decimal voucherDetailId, string concept,
                    decimal debit, decimal credit, decimal accountId, bool isActive)
        {
            _procedureName = "sp_detalleComprobante";
            _voucherId = voucherId;
            _voucherDetailId = voucherDetailId;
            _concept = concept;
            _debit = debit;
            _credit = credit;
            _accountId = accountId;
            _isActive = isActive;
        }



        protected override void setCatalog(DataAccess dataAccess, VoucherDetail data)
        {
            dataAccess.AdParameter("@i_idComprobante", DBTypeEnum.Numeric, data.VoucherId);
            dataAccess.AdParameter("@i_idDetalleComprobante", DBTypeEnum.Numeric, data.VoucherDetailId);
            dataAccess.AdParameter("@i_concepto", DBTypeEnum.VarChar, data.Concept);            
            dataAccess.AdParameter("@i_debe", DBTypeEnum.Decimal, data.Debit);
            dataAccess.AdParameter("@i_haber", DBTypeEnum.Decimal, data.Credit);
            dataAccess.AdParameter("@i_idCuenta", DBTypeEnum.Numeric, data.AccountId);
            dataAccess.AdParameter("@i_es_activo", DBTypeEnum.VarChar, data.IsActive ? "S" : "N");
        }

        protected override VoucherDetail getNewCatalog()
        {
            return new VoucherDetail();
        }

        protected override VoucherDetail getCatalog(DataRow row)
        {
            var result = new VoucherDetail(decimal.Parse(row[0].ToString()), decimal.Parse(row[1].ToString()),
                                   row[2].ToString(),
                                   decimal.Parse(row[3].ToString()), decimal.Parse(row[4].ToString()),
                                   decimal.Parse(row[5].ToString()), row[6].ToString() == "S");
            return result;
        }       

        protected override string getBulkRecord(VoucherDetail voucherDetail)
        {
            var _delimiter = "þ";
            return string.Concat(voucherDetail.VoucherId, _delimiter, voucherDetail.VoucherDetailId, _delimiter, voucherDetail.Concept, _delimiter, 
                voucherDetail.Debit.ToString(), _delimiter, voucherDetail.Credit.ToString(),voucherDetail.AccountId);
        }

        public List<VoucherDetail> getDetailList(decimal voucherId, decimal voucherDetailId)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "S");
            dataAccess.AdParameter("@i_idComprobante", DBTypeEnum.Numeric, voucherId);
            dataAccess.AdParameter("@i_idDetalleComprobante", DBTypeEnum.Numeric, voucherDetailId);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, _procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                }
                else
                {
                    response = new DataTable();
                }
            }
            List<VoucherDetail> catalogList = new List<VoucherDetail>();
            if (response.Rows == null)
            {
                return catalogList;
            }
            foreach (DataRow row in response.Rows)
            {
                VoucherDetail catalog = getCatalog(row);
                catalogList.Add(catalog);
            }
            return catalogList;

        }
    }
}
