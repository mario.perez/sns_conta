﻿using dataAccess.BaseManagement;
using dataAccess.SqlDatabaseAccess;
using System.Data;

namespace businessLogic.Core
{
    public class DocumentType: CustomCatalogManager<DocumentType>, ICRUD<DocumentType>
    {
        #region Private Declarations...
        private string _documentTypeId;
        private string _documentTypeName;
        private bool _isActive;
        #endregion

        #region Public Attribute...
        public string DocumentTypeId { get => _documentTypeId; }
        public string DocumentTypeName { get => _documentTypeName; }
        public bool IsActive { get => _isActive; }
        #endregion

        public DocumentType()
        {
            _procedureName = "sp_tipoDocumento";
        }

        public DocumentType(string documentTypeId, string documentTypeName, bool isActive)
        {
            _procedureName = "sp_tipoDocumento";
            _documentTypeId = documentTypeId;
            _documentTypeName = documentTypeName;
            _isActive = isActive;
        }
        protected override void setCatalog(DataAccess dataAccess, DocumentType data)
        {
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, data.DocumentTypeId);
            dataAccess.AdParameter("@i_nombre", DBTypeEnum.VarChar, data.DocumentTypeName);
            dataAccess.AdParameter("@i_es_activo", DBTypeEnum.VarChar, data.IsActive ? "S" : "N");
        }

        protected override DocumentType getNewCatalog()
        {
            return new DocumentType();
        }

        protected override DocumentType getCatalog(DataRow row)
        {
            var result = new DocumentType(row[0].ToString(), row[1].ToString(), row[2].ToString() == "S");
            return result;
        }



    }
}
