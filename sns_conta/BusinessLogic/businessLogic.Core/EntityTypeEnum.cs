﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace businessLogic.Core
{
    public enum EntityTypeEnum
    {
        [Display(Name = "Jurídica")]
        JURIDICA,
        [Display(Name = "Natural")]
        NATURAL
    }
}
