﻿using businessLogic.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webUI.SICO.Models;

namespace webUI.SICO.Controllers
{
    public class VoucherController : Controller
    {
        //buscar por tipo, por numero, por fecha.
        // Debe tener las opciones: Crear comprobante, Buscar comprobante, ver comprobante, editar comprobante, anular comprobante. 
        // GET: Voucher
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }


        public ActionResult Details(string Id)
        {
            VoucherViewModel result = new VoucherViewModel();
            var data = new Voucher().GetOne(Id);
            result.VoucherTypeId = data.VoucherTypeId;
            result.VoucherId = data.VoucherId;
            result.VoucherDate = data.VoucherDate;
            result.IsEquivalent = data.IsEquivalent;
            result.IsActive = data.IsActive;
            result.Debit = data.Debit;
            result.Credit = data.Credit;
            result.CreationDate = data.CreationDate;
            result.Concept = data.Concept;
            result.Comments = data.Comments;
            var details = new VoucherDetail().getDetailList(decimal.Parse(Id), 0);
            result.Details = new List<VoucherDetailViewModel>();
            foreach(var item in details)
            {
                result.Details.Add(new VoucherDetailViewModel(
                        item.VoucherId, 
                        item.VoucherDetailId, 
                        item.Concept, 
                        item.Debit, 
                        item.Credit, 
                        item.AccountId, 
                        item.IsActive                       
                    ));
            }
            return View(result);
        }

        [HttpPost]
        public JsonResult AddNewVoucher(VoucherViewModel param)
        {
            ResponseModel result = new ResponseModel();

            try
            {
                List<VoucherDetail> details = new List<VoucherDetail>();
                foreach (var item in param.Details)
                {
                    details.Add(new VoucherDetail(item.VoucherId, item.VoucherDetailId, item.Concept, item.Debit, item.Credit, 
                                                item.AccountId, item.IsActive));
                }
                Voucher request = new Voucher(param.VoucherId, param.VoucherDate, param.CreationDate.ToString(), param.Concept, param.Debit, param.Credit, 
                                            param.IsActive, param.Comments, param.VoucherTypeId, param.IsActive, details.ToArray(),param.Beneficiary);
                string outputCode;
                var response = new Voucher().SaveOneShot(request,out outputCode);
                if (response)
                {
                    result = new ResponseModel() { Id = "0", Message = outputCode, Type = 'S' };
                }
                else
                {
                    result = new ResponseModel() { Id = "1", Message = "Fallo al crear el comprobante en BD", Type = 'E' };
                }
                
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Search()
        {
            return View();
        }


        [HttpPost]
        public JsonResult GetFilteredVouchers(VoucherFilterViewModel param)
        {
            var viewData = new VoucherListViewModel();
            var result = new List<VoucherViewModel>();            
            try
            {
                var vouchers = new Voucher().GetFiltered(param.VoucherTypeId, param.VoucherId, param.InitialDate, 
                                                            param.FinalDate, param.Concept, param.Beneficiary);
                foreach (var v in vouchers)
                {
                    result.Add(new VoucherViewModel(v.VoucherId,v.VoucherDate,v.CreationDate, v.Concept,
                                                    v.Debit,v.Credit,v.IsEquivalent,v.Comments,
                                                    v.VoucherTypeId, v.IsActive,v.Beneficiary));
                }
                viewData.Info = result.ToArray();
            }
            catch (Exception ex)
            {
                viewData.Type = 'E';
                viewData.Message = ex.Message;
                viewData.Id = "404";
            }
            return Json(viewData, JsonRequestBehavior.AllowGet);
        }


    }
}