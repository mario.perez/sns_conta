﻿using businessLogic.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webUI.SICO.Models;

namespace webUI.SICO.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAccounts()
        {
            var viewData = new AccountListViewModel();
            var list = new List<AccountViewModel>();
            try
            {
                var response = new AccountingCatalog().GetAll("0");
                foreach (var item in response)
                {
                    list.Add(new AccountViewModel(item.AccountId.ToString(), item.AccountingChain,
                        item.AccountName, item.Level, item.ParentAccountId.ToString(), item.IsActive));

                }
                viewData.Info = list.ToArray();
            }
            catch (Exception ex)
            {
                viewData.Type = 'E';
                viewData.Message = ex.Message;
                viewData.Id = "404";
            }
            return Json(viewData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddNewAccount(AccountViewModel param)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                var response = new AccountingCatalog().Add(new AccountingCatalog(decimal.Parse(param.Id), param.Name,
                    param.AccountingChain, param.Level, decimal.Parse(param.ParentId), true));
                result = new ResponseModel() { Id = "0", Message = string.Empty, Type = 'S' };
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateAccount(AccountViewModel param)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                var response = new AccountingCatalog().Update(new AccountingCatalog(decimal.Parse(param.Id), param.Name,
                    param.AccountingChain, param.Level, decimal.Parse(param.ParentId), true));
                result = new ResponseModel() { Id = "0", Message = string.Empty, Type = 'S' };
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //DeleteAccount
        [HttpPost]
        public JsonResult DeleteAccount(AccountViewModel param)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                var response = new AccountingCatalog().Remove(new AccountingCatalog(decimal.Parse(param.Id), param.Name,
                    param.AccountingChain, param.Level, decimal.Parse(param.ParentId), true));
                result = new ResponseModel() { Id = "0", Message = string.Empty, Type = 'S' };
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}