﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webUI.SICO.Models;
using businessLogic.Core;

namespace webUI.SICO.Controllers
{
    public class DocumentTypeController : Controller
    {
        // GET: DocumentType
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            var result = new List<CatalogStringViewModel>();
            var documentTypes = new DocumentType().GetAll("");
            foreach (var dt in documentTypes)
            {
                result.Add(new CatalogStringViewModel(dt.DocumentTypeId, dt.DocumentTypeName));
            }
            return View(result);
        }

        [HttpPost]
        public JsonResult Save(CatalogStringViewModel param)
        {

            ResponseModel result = new ResponseModel();
            try
            {
                var data = new DocumentType(param.Code, param.Value, true);
                var response = data.Add(data);
                result = new ResponseModel() { Id = "0", Message = string.Empty, Type = 'S' };
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Delete(CatalogStringViewModel param)
        {

            ResponseModel result = new ResponseModel();
            try
            {
                var data = new DocumentType(param.Code, "", true);
                var response = data.Remove(data);
                result = new ResponseModel() { Id = "0", Message = string.Empty, Type = 'S' };
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Edit(CatalogStringViewModel param)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                var data = new DocumentType(param.Code, param.Value, true);
                var response = data.Update(data);
                result = new ResponseModel() { Id = "0", Message = string.Empty, Type = 'S' };
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}