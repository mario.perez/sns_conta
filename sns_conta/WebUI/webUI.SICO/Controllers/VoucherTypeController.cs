﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using webUI.SICO.Models;

namespace webUI.SICO.Controllers
{
    public class VoucherTypeController : Controller
    {
        // GET: VoucherType
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            var result = new List<CatalogStringViewModel>();
            var voucherTypes = new businessLogic.Core.VoucherType().GetAll("");
            foreach (var vt in voucherTypes)
            {
                result.Add(new CatalogStringViewModel(vt.VoucherTypeId, vt.VoucherTypeName));
            }
            return View(result);
        }

        [HttpGet]
        public JsonResult GetVoucherTypes()
        {
            var viewData = new VoucherTypeListViewModel();
            var result = new List<CatalogStringViewModel>();
            try
            {
                var voucherTypes = new businessLogic.Core.VoucherType().GetAll("");
                foreach (var vt in voucherTypes)
                {
                    result.Add(new CatalogStringViewModel(vt.VoucherTypeId, vt.VoucherTypeName));
                }
                viewData.Info = result.ToArray();
            }
            catch (Exception ex)
            {
                viewData.Type = 'E';
                viewData.Message = ex.Message;
                viewData.Id = "404";
            }
            return Json(viewData, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Save(CatalogStringViewModel param)
        {

            ResponseModel result = new ResponseModel();
            try
            {
                var data = new businessLogic.Core.VoucherType(param.Code, param.Value, true);
                var response = data.Add(data);
                result = new ResponseModel() { Id = "0", Message = string.Empty, Type = 'S' };
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Delete(CatalogStringViewModel param)
        {

            ResponseModel result = new ResponseModel();
            try
            {
                var data = new businessLogic.Core.VoucherType(param.Code, "", true);
                var response = data.Remove(data);
                result = new ResponseModel() { Id = "0", Message = string.Empty, Type = 'S' };
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Edit(CatalogStringViewModel param)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                var data = new businessLogic.Core.VoucherType(param.Code, param.Value, true);
                var response = data.Update(data);
                result = new ResponseModel() { Id = "0", Message = string.Empty, Type = 'S' };
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


    }
}