﻿using businessLogic.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using webUI.SICO.Models;

namespace webUI.SICO.Controllers
{
    public class EntityController : Controller
    {
        //TODO: Cambiar forma de edicion y mostrar detalles en EntityPerson
        // GET: EntityPerson
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            var result = new List<EntityViewModel>();
            var companies = new businessLogic.Core.EntityPerson().GetAll("");
            foreach (var company in companies)
            {
                result.Add(new EntityViewModel(company.Id, company.Name,company.Document, company.EntityType, 
                    company.DocumentType.ToString(),company.Address, company.Telephone, company.IsActive));
            }
            return View(result);
        }

        public ActionResult Details(string Id)
        {
            var documentTypes = new DocumentType().GetAll(string.Empty);
            var company = new EntityPerson().GetOne(Id);
            string documentSelected = (from e in documentTypes where e.DocumentTypeId == company.DocumentType.ToString() select e.DocumentTypeName).FirstOrDefault();
            var result = new EntityViewModel(company.Id, company.Name, company.Document, company.EntityType,
                documentSelected, company.Address, company.Telephone, company.IsActive);
            return View(result);
        }

        public ActionResult AddNew()
        {
            ViewBag.DocumentTypes = new DocumentType().GetAll(string.Empty);
            return View(new EntityViewModel());
        }

        public ActionResult EditOne(string Id)
        {
            ViewBag.DocumentTypes = new DocumentType().GetAll(string.Empty);
            var company = new EntityPerson().GetOne(Id);
            var result = new EntityViewModel(company.Id, company.Name, company.Document, company.EntityType,
                company.DocumentType.ToString(), company.Address, company.Telephone, company.IsActive);
            return View(result);
        }


        [HttpPost]
        public JsonResult Save(EntityViewModel param)
        {

            ResponseModel result = new ResponseModel();
            try
            {
                var data = new businessLogic.Core.EntityPerson(param.Id,param.Name, param.Document, 
                    Enum.GetName(typeof(EntityTypeEnum),param.EntityType),param.DocumentType,param.Address,param.Telephone, true);
                var response = data.Add(data);
                result = new ResponseModel() { Id = "0", Message = string.Empty, Type = 'S' };                
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Delete(EntityViewModel param)
        {

            ResponseModel result = new ResponseModel();
            try
            {
                var data = new businessLogic.Core.EntityPerson(param.Id, "", "", "", "-1", "", "", true);
                var response = data.Remove(data);
                result = new ResponseModel() { Id = "0", Message = string.Empty, Type = 'S' };
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Edit(EntityViewModel param)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                var data = new businessLogic.Core.EntityPerson(param.Id, param.Name, param.Document, Enum.GetName(typeof(EntityTypeEnum), param.EntityType)
                    , param.DocumentType, param.Address, param.Telephone, true);
                var response = data.Update(data);
                result = new ResponseModel() { Id = "0", Message = string.Empty, Type = 'S' };
            }
            catch (Exception ex)
            {
                result = new ResponseModel() { Id = "1", Message = ex.Message, Type = 'E' };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


    }
}