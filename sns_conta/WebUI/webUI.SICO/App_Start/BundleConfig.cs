﻿using System.Web;
using System.Web.Optimization;

namespace webUI.SICO
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/conta").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/jquery.js",
                      "~/Scripts/bootstrap.bundle.min.js",
                      "~/Scripts/jquery.scrollTo.min.js",
                      "~/Scripts/jquery.nicescroll.js",
                      "~/Scripts/respond.min.js",
                      "~/Scripts/jquery.pulsate.min.js"
                      ));

            /*bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));*/
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.min.css",
                "~/Content/bootstrap-reset.css",
                "~/Content/font-awesome/css/font-awesome.css",
                "~/Content/slidebars.css",
                "~/Content/style.css",
                "~/Content/style-responsive.css",                
                "~/Content/gritter/css/jquery.gritter.css"
                ));

        }
    }
}
