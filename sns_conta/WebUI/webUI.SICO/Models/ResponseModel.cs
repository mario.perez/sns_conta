﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webUI.SICO.Models
{
    public class ResponseModel
    {
        public string Id { get; set; }
        public string Message { get; set; }
        public char Type { get; set; }
    }
}