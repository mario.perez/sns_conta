﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webUI.SICO.Models
{
    public class VoucherDetailViewModel
    {
        public decimal VoucherId { get; set; }
        public decimal VoucherDetailId { get; set; }
        public string Concept { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal AccountId { get; set; }
        public bool IsActive { get; set; }

        public VoucherDetailViewModel() { }

        public VoucherDetailViewModel(decimal voucherId, decimal voucherDetailId, string concept, decimal debit, decimal credit, decimal accountId, bool isActive)
        {
            VoucherId = voucherId;
            VoucherDetailId = voucherDetailId;
            Concept = concept;
            Debit = debit;
            Credit = credit;
            AccountId = accountId;
            IsActive = isActive;
        }
    }
}