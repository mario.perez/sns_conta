﻿namespace webUI.SICO.Models
{
    public class CatalogStringViewModel
    {
        public string Code{ get; set; }
        public string Value { get; set; }

        public CatalogStringViewModel()
        {

        }
        public CatalogStringViewModel(string code, string value)
        {
            Code = code;
            Value = value;
        }
    }
}