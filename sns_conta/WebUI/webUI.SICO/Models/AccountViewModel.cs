﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webUI.SICO.Models
{
    public class AccountViewModel
    {
        public string Id { get; set; }
        public string AccountingChain { get; set; }
        public string Name{ get; set; }
        public int Level { get; set; }
        public string ParentId { get; set; }

        public bool Active { get; set; }

        public AccountViewModel() { }

        public AccountViewModel(string id, string accountingChain, string name, int level, string parentId, bool active)
        {
            Id = id;
            AccountingChain = accountingChain;
            Name = name;
            Level = level;
            ParentId = parentId;
            Active = active;
        }

    }
}