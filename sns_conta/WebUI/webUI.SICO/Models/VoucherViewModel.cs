﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webUI.SICO.Models
{
    public class VoucherViewModel
    {
        public decimal VoucherId { get; set; }
        public DateTime VoucherDate { get; set; }
        public string CreationDate { get; set; }
        public string Concept { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public bool IsEquivalent { get; set; }
        public string Comments { get; set; }
        public string VoucherTypeId { get; set; }
        public bool IsActive { get; set; }
        public string Beneficiary { get; set; }
        public List<VoucherDetailViewModel> Details { get; set; }

        public string VoucherDatestr { get => VoucherDate.ToString("dd/MM/yyyy"); }
        public VoucherViewModel() { }

        public VoucherViewModel(decimal voucherId, DateTime voucherDate, string creationDate, string concept, decimal debit,
                                decimal credit, bool isEquivalent, string comments, string voucherTypeId, bool isActive, string beneficiary)
        {
            VoucherId = voucherId;
            VoucherDate = voucherDate;
            CreationDate = creationDate;
            Concept = concept;
            Debit = debit;
            Credit = credit;
            IsEquivalent = isEquivalent;
            Comments = comments;
            VoucherTypeId = voucherTypeId;
            IsActive = isActive;
            Beneficiary = beneficiary;
            Details = new List<VoucherDetailViewModel>();
        }

    }
}