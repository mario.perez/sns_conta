﻿using businessLogic.Core;
using System.ComponentModel.DataAnnotations;

namespace webUI.SICO.Models
{
    public class EntityViewModel
    {
        [Display(Name = "Código Interno")]
        public int Id { get; set; }
        [Display(Name = "Nombre")]
        public string Name { get; set; }
        [Display(Name = "Número Identificación")]
        public string Document { get; set; }
        [Display(Name = "Tipo Entidad")]
        public EntityTypeEnum EntityType { get; set; }
        [Display(Name = "Tipo Identificación")]
        public string DocumentType { get; set; }
        [Display(Name = "Dirección")]
        public string Address { get; set; }
        [Display(Name = "Teléfono")]
        public string Telephone { get; set; }
        [Display(Name = "Activo")]
        public bool IsActive { get; set; }

        public EntityViewModel()
        {

        }

        public EntityViewModel(int id, string name, string document, string entityType, string documentType, string address, string telephone,bool isActive)
        {
            Id = id;
            Name = name;
            Document = document;
            EntityType = entityType== "JURIDICA"?EntityTypeEnum.JURIDICA:EntityTypeEnum.NATURAL;
            DocumentType = documentType;
            Address = address;
            Telephone = telephone;
            IsActive = isActive;
        }


    }
}