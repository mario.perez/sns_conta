﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webUI.SICO.Models
{
    public class VoucherFilterViewModel
    {
        public decimal VoucherId { get; set; }
        public string VoucherTypeId { get; set; }
        public DateTime InitialDate { get; set; }
        public DateTime FinalDate { get; set; }
        public string Beneficiary { get; set; }
        public string Concept { get; set; }
    }
}