USE [sns_conta]
GO
/****** Object:  StoredProcedure [dbo].[sp_comprobante]    Script Date: 6/9/2018 9:30:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.sp_comprobante') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_comprobante
    IF OBJECT_ID('dbo.sp_comprobante') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_comprobante >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_comprobante >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <08/12/2020>
-- Description:	<SCRUD para Comprobantes.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_comprobante]
	@i_user          		varchar(32),
	@i_terminal      		varchar(32),
	@i_operacion     		char(1),
	@i_codigo        		numeric(18,0) = -1,
	@i_fechaComprobante 	varchar(32) = '',
	@i_fechaComprobanteFin	varchar(32) = '',
	@i_concepto        		varchar(max) = '',	
	@i_beneficiario    		varchar(3000) = '',	
	@i_debe   				numeric(16,2) = 0,	
	@i_haber   				numeric(16,2) = 0,	
	@i_esEquivalente   		char(1) = '',	
	@i_observaciones   		varchar(max) = '',	
	@i_idTipoComprobante	varchar(5) = '',	
	@i_es_activo     		char(1) = '',
	@o_codigo     			numeric(18,0) = -1 output
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
	
		SELECT [IdComprobante]
			, convert(varchar,FechaComprobante,103)
			, convert(varchar,FechaCreacion,103) + ' ' + convert(varchar,FechaCreacion,108)
			,[Concepto]
			,[Debe]
			,[Haber]
			,[EsEquivalente]
			,[Observaciones]
			,[IdTipoComprobante]
			,case EsActivo when 1 then 'S' else 'N' end as EsActivo
			,isnull(Beneficiario,'') as Beneficiario
		FROM [dbo].[Comprobante]
		WHERE EsActivo = 1
		AND IdComprobante > @i_codigo
		ORDER BY IdComprobante asc

		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		SELECT [IdComprobante]
			, convert(varchar,FechaComprobante,103)
			, convert(varchar,FechaCreacion,103) + ' ' + convert(varchar,FechaCreacion,108)
			,[Concepto]
			,[Debe]
			,[Haber]
			,[EsEquivalente]
			,[Observaciones]
			,[IdTipoComprobante]
			,case EsActivo when 1 then 'S' else 'N' end as EsActivo
			,isnull(Beneficiario,'') as Beneficiario
		FROM [dbo].[Comprobante]
		WHERE EsActivo = 1
		AND IdComprobante = @i_codigo

		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
	
		DECLARE @w_fechaCreacion datetime
		
		set @w_fechaCreacion = getdate()
		
		/*IF @i_codigo = -1 
		BEGIN*/
			set @i_codigo = 0
			
			Select @i_codigo = max(IdComprobante) from Comprobante
			
			Set @i_codigo = isnull(@i_codigo,0) + 1
		--END


		INSERT INTO [dbo].[Comprobante]
				([IdComprobante],[FechaComprobante],[FechaCreacion]		--1
				,[Concepto],[Debe],[Haber]								--2
				,[EsEquivalente],[Observaciones],[IdTipoComprobante]	--3
				,[EsActivo], [Beneficiario])							--4
			VALUES
				(@i_codigo,@i_fechaComprobante,@w_fechaCreacion			--1
				,@i_concepto,@i_debe ,@i_haber							--2
				,@i_esEquivalente,@i_observaciones,@i_idTipoComprobante --3
				,1, @i_beneficiario)									--4

		set @o_codigo = @i_codigo
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN

		UPDATE [dbo].[Comprobante]
		SET [FechaComprobante] = convert(datetime,@i_fechaComprobante,103)
			,[Concepto] = @i_concepto
			,[Debe] = @i_debe
			,[Haber] = @i_haber
			,[EsEquivalente] = case @i_esEquivalente when 'S' then 1 else 0 end 
			,[Observaciones] = @i_observaciones
			,[IdTipoComprobante] = @i_idTipoComprobante		
			,[Beneficiario] = @i_beneficiario
		WHERE [IdComprobante] = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE [dbo].[Comprobante]
		set [EsActivo] = 0			
		WHERE [IdComprobante] = @i_codigo
		
		return 0
	END

	IF @i_operacion = 'F'
	BEGIN
		Declare @txtsql  nvarchar(max) 

		set @txtsql = 'SELECT top 50 [IdComprobante]
		, convert(varchar,FechaComprobante,103) as FechaComprobante
		, convert(varchar,FechaCreacion,103) + '' '' + convert(varchar,FechaCreacion,108) as FechaCreacion
		,[Concepto],[Debe]
			,[Haber],[EsEquivalente],[Observaciones],[IdTipoComprobante]
			,case EsActivo when 1 then ''S'' else ''N'' end as EsActivo
			,isnull(Beneficiario,'''') as Beneficiario
		FROM [dbo].[Comprobante]
		WHERE 1=1'

		if @i_codigo>0 		
			set @txtsql = @txtsql + ' AND IdComprobante >= ' + convert(varchar,@i_codigo)
		
		if len(isnull(@i_idTipoComprobante,'')) > 0
		    set @txtsql = @txtsql + ' AND IdTipoComprobante = ''' + @i_idTipoComprobante + ''''

		if len(isnull(@i_fechaComprobante,''))>0 and len(isnull(@i_fechaComprobanteFin,''))>0
			set @txtsql = @txtsql + ' AND FechaComprobante >= ''' + @i_fechaComprobante + 
						''' AND FechaComprobante <=''' + @i_fechaComprobanteFin + ''''

		if len(isnull(@i_concepto,'')) > 0
			set @txtsql = @txtsql + ' AND Concepto like ''' + @i_concepto + ''''


		set @txtsql = @txtsql + 'ORDER BY IdComprobante asc'

		EXECUTE sp_executesql @txtsql,N'';

	END


END
GO


IF OBJECT_ID('dbo.sp_comprobante') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_comprobante') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_comprobante >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_comprobante >>>'
END
go

