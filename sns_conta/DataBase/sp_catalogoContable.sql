USE [sns_conta]
GO
/****** Object:  StoredProcedure [dbo].[sp_catalogoContable]    Script Date: 6/9/2018 9:30:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.sp_catalogoContable') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_catalogoContable
    IF OBJECT_ID('dbo.sp_catalogoContable') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_catalogoContable >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_catalogoContable >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <08/12/2020>
-- Description:	<SCRUD para Catalogo Contable.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_catalogoContable]
	@i_user          	varchar(32),
	@i_terminal      	varchar(32),
	@i_operacion     	char(1),
	@i_codigo        	numeric(18,0) = -1,
	@i_nombre        	varchar(500) = '',	
	@i_cadenaContable   varchar(100) = '',	
	@i_nivel        	int = -1,	
	@i_idCuentaPadre    numeric(18,0) = -1,	
	@i_es_activo     	char(1) = ''
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		SELECT [IdCuenta]
			  ,[NombreCuenta]
			  ,[CadenaContable]
			  ,[Nivel]
			  ,[IdCuentaPadre]
			  ,case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM [dbo].[CatalogoContable]
		WHERE IdCuenta > @i_codigo
		ORDER BY CadenaContable asc

		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		SELECT [IdCuenta]
			  ,[NombreCuenta]
			  ,[CadenaContable]
			  ,[Nivel]
			  ,[IdCuentaPadre]
			  ,case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM [dbo].[CatalogoContable]
		WHERE IdCuenta = @i_codigo
		
		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		set @i_codigo = 0
		
		Select @i_codigo = max(IdCuenta) from CatalogoContable
		
		Set @i_codigo = isnull(@i_codigo,0) + 1
		
		INSERT INTO [dbo].[CatalogoContable]
           ([IdCuenta],[NombreCuenta],[CadenaContable]	--1
           ,[Nivel],[IdCuentaPadre],[EsActivo])			--2
		VALUES
           (@i_codigo,@i_nombre,@i_cadenaContable 		--1
           ,@i_nivel, @i_idCuentaPadre,1)				--2
				 
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN

		UPDATE [dbo].[CatalogoContable]
		SET [NombreCuenta] = @i_nombre
			,[CadenaContable] = @i_cadenaContable
			,[Nivel] = @i_nivel
			,[IdCuentaPadre] = @i_idCuentaPadre			
		WHERE [IdCuenta] = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE CatalogoContable
		set EsActivo = 0			
		WHERE IdCuenta = @i_codigo
		
		return 0
	END

END
GO


IF OBJECT_ID('dbo.sp_catalogoContable') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_catalogoContable') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_catalogoContable >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_catalogoContable >>>'
END
go

