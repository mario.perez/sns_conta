USE [sns_conta]
GO
/****** Object:  Table [dbo].[CatalogoContable]    Script Date: 23/04/2021 6:40:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CatalogoContable](
	[IdCuenta] [numeric](18, 0) NOT NULL,
	[NombreCuenta] [varchar](500) NOT NULL,
	[CadenaContable] [varchar](100) NOT NULL,
	[Nivel] [int] NOT NULL,
	[IdCuentaPadre] [numeric](18, 0) NOT NULL,
	[EsActivo] [bit] NOT NULL,
 CONSTRAINT [PK_CatalogoContable] PRIMARY KEY CLUSTERED 
(
	[IdCuenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comprobante]    Script Date: 23/04/2021 6:40:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comprobante](
	[IdComprobante] [numeric](18, 0) NOT NULL,
	[FechaComprobante] [date] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[Concepto] [varchar](max) NOT NULL,
	[Debe] [numeric](16, 2) NOT NULL,
	[Haber] [numeric](16, 2) NOT NULL,
	[EsEquivalente] [char](1) NOT NULL,
	[Observaciones] [varchar](max) NOT NULL,
	[IdTipoComprobante] [varchar](5) NOT NULL,
	[EsActivo] [bit] NOT NULL,
	[Beneficiario] [varchar](3000) NULL,
 CONSTRAINT [PK_Comprobante] PRIMARY KEY CLUSTERED 
(
	[IdComprobante] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleComprobante]    Script Date: 23/04/2021 6:40:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleComprobante](
	[IdComprobante] [numeric](18, 0) NOT NULL,
	[IdDetalleComprobante] [numeric](18, 0) NOT NULL,
	[Concepto] [varchar](1000) NOT NULL,
	[Debe] [numeric](16, 2) NULL,
	[Haber] [numeric](16, 2) NULL,
	[IdCuenta] [numeric](18, 0) NULL,
	[EsActivo] [bit] NOT NULL,
 CONSTRAINT [PK_DetalleComprobante] PRIMARY KEY CLUSTERED 
(
	[IdComprobante] ASC,
	[IdDetalleComprobante] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Entidad]    Script Date: 23/04/2021 6:40:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entidad](
	[IdEntidad] [int] NOT NULL,
	[NombreEntidad] [varchar](500) NOT NULL,
	[TipoEntidad] [varchar](16) NOT NULL,
	[Documento] [varchar](64) NOT NULL,
	[TipoDocumento] [int] NOT NULL,
	[Direccion] [varchar](max) NULL,
	[Telefono] [varchar](32) NULL,
	[EsActivo] [bit] NOT NULL,
 CONSTRAINT [PK_Entidad] PRIMARY KEY CLUSTERED 
(
	[IdEntidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Parametros]    Script Date: 23/04/2021 6:40:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Parametros](
	[Codigo] [varchar](32) NOT NULL,
	[Valor] [varchar](max) NOT NULL,
	[Descripcion] [varchar](256) NOT NULL,
	[EsEditable] [bit] NOT NULL,
	[Opciones] [varchar](2000) NOT NULL,
	[GrupoConfig] [varchar](32) NOT NULL,
	[TipoDato] [varchar](100) NOT NULL,
 CONSTRAINT [UC_Parametros_1] UNIQUE NONCLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoComprobante]    Script Date: 23/04/2021 6:40:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoComprobante](
	[IdTipoComprobante] [varchar](5) NOT NULL,
	[NombreTipoComprobantes] [varchar](250) NOT NULL,
	[EsActivo] [bit] NOT NULL,
 CONSTRAINT [PK_TipoComprobante] PRIMARY KEY CLUSTERED 
(
	[IdTipoComprobante] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoDocumento]    Script Date: 23/04/2021 6:40:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoDocumento](
	[IdTipoDocumento] [varchar](5) NOT NULL,
	[NombreTipoDocumentos] [varchar](250) NOT NULL,
	[EsActivo] [bit] NOT NULL,
 CONSTRAINT [PK_TipoDocumento] PRIMARY KEY CLUSTERED 
(
	[IdTipoDocumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Comprobante] ADD  CONSTRAINT [DF_Comprobante_Debe]  DEFAULT ((0)) FOR [Debe]
GO
ALTER TABLE [dbo].[Comprobante] ADD  CONSTRAINT [DF_Comprobante_Haber]  DEFAULT ((0)) FOR [Haber]
GO
