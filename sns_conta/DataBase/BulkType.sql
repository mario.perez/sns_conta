USE sns_conta
GO

IF EXISTS(SELECT 1 
		  FROM systypes
		  WHERE name = 'BulkType')
BEGIN
    DROP TYPE BulkType
    IF EXISTS(SELECT 1 FROM systypes
		          where name = 'BulkType')
        PRINT N'<<< FAILED DROPPING TYPE BulkType >>>'
    ELSE
        PRINT N'<<< DROPPED TYPE BulkType >>>'
END
go
CREATE TYPE [dbo].[BulkType] AS TABLE(
	[Record] [varchar](max) NOT NULL
)
GO
IF EXISTS(SELECT 1 
		  FROM systypes
		  where name = 'BulkType')
    PRINT N'<<< CREATED TYPE BulkType >>>'
ELSE
    PRINT N'<<< FAILED CREATING TYPE BulkType >>>'
go
