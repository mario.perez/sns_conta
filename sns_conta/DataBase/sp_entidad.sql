USE [sns_conta]
GO
/****** Object:  StoredProcedure [dbo].[sp_entidad]    Script Date: 6/9/2018 9:30:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.sp_entidad') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_entidad
    IF OBJECT_ID('dbo.sp_entidad') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_entidad >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_entidad >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <08/12/2020>
-- Description:	<SCRUD para Entidad.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_entidad]
	@i_user          varchar(32),
	@i_terminal      varchar(32),
	@i_operacion     char(1),
	@i_codigo        int = -1,
	@i_nombre        varchar(500) = '',	
	@i_tipoEntidad    varchar(16)  = '',
	@i_documento      varchar(64)  = '',
	@i_tipoDocumento  int  = -1,
	@i_direccion     varchar(max) = '',
	@i_telefono      varchar(32)  = '',
	@i_es_activo     char(1) = ''
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		select  IdEntidad,		--0
				NombreEntidad,	--1	
				TipoEntidad,    --2
				Documento,      --3
				TipoDocumento,  --4
				Direccion,      --5
				Telefono,       --6
				case EsActivo when 1 then 'S' else 'N' end as EsActivo --7
		from Entidad
		where EsActivo = 1
		and IdEntidad > @i_codigo
		order by IdEntidad asc
		
		return 0
	END
	
	
	
	IF @i_operacion = 'C'
	BEGIN
		select  IdEntidad,
				NombreEntidad,	
				TipoEntidad,
				Documento,
				TipoDocumento,				
				Direccion,
				Telefono,
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from Entidad
		where EsActivo = 1
		and IdEntidad = @i_codigo
		
		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		set @i_codigo = 0
		
		Select @i_codigo = max(IdEntidad) from Entidad
		
		Set @i_codigo = isnull(@i_codigo,0)
		
		INSERT INTO  Entidad(IdEntidad, NombreEntidad, TipoEntidad, Documento, TipoDocumento,Direccion,Telefono,EsActivo)
		VALUES (@i_codigo+1, @i_nombre, @i_tipoEntidad, @i_documento, @i_tipoDocumento, @i_direccion, @i_telefono,1)
		 		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE Entidad
		set NombreEntidad = @i_nombre,
			TipoEntidad = @i_tipoEntidad,
			Documento = @i_documento,
			TipoDocumento = @i_tipoDocumento,
			Direccion = @i_direccion,
			Telefono = @i_telefono
		WHERE IdEntidad = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE Entidad
		set EsActivo = 0			
		WHERE IdEntidad = @i_codigo
		
		return 0
	END

END
GO


IF OBJECT_ID('dbo.sp_entidad') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_entidad') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_entidad >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_entidad >>>'
END
go

