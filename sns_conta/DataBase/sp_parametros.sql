USE [sns_conta]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.sp_parametros') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_parametros
    IF OBJECT_ID('dbo.sp_parametros') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_parametros >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_parametros >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <08/12/2020>
-- Description:	<SCRUD para Parametros.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_parametros]
	@i_user          		varchar(32),
	@i_terminal      		varchar(32),
	@i_operacion            varchar(1),
	@i_codigo				varchar(32) = '',
	@i_valor                varchar(max) = '',			
	@i_grupoConfig          varchar(32) = ''		
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
	
		SELECT  [Codigo]      
		       ,[Valor]       
		       ,[Descripcion] 
		       ,CASE EsEditable WHEN 1 THEN 'S' ELSE 'N' END  as EsEditable
	           ,[Opciones]    
		       ,[GrupoConfig] 
               ,[TipoDato]    
		FROM Parametros
		WHERE EsEditable = 1
		and GrupoConfig = @i_grupoConfig
		Order by Codigo asc		
		
		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
	
		SELECT  [Codigo]      
		       ,[Valor]       
		       ,[Descripcion] 
		       ,CASE EsEditable WHEN 1 THEN 'S' ELSE 'N' END  as EsEditable
	           ,[Opciones]    
		       ,[GrupoConfig] 
               ,[TipoDato]    
		FROM Parametros
		WHERE EsEditable = 1
		and Codigo = @i_codigo		
		Order by Codigo asc		
		
		return 0
	
	END
	
	IF @i_operacion = 'U'
	BEGIN
		UPDATE Parametros
		SET Valor = @i_valor
		WHERE Codigo = @i_codigo
	
		return 0
	END
	
	
	
	
	
	
	
	
	

	RAISERROR('El valor en el parametro %s es incorrecto',16,1,'@i_operacion');
END
GO


IF OBJECT_ID('dbo.sp_parametros') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_parametros') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_parametros >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_parametros >>>'
END
go

