USE [sns_conta]
GO
/****** Object:  StoredProcedure [dbo].[sp_tipoDocumento]    Script Date: 6/9/2018 9:30:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.sp_tipoDocumento') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_tipoDocumento
    IF OBJECT_ID('dbo.sp_tipoDocumento') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_tipoDocumento >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_tipoDocumento >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <08/12/2020>
-- Description:	<SCRUD para Tipo Comprobante.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_tipoDocumento]
	@i_user          varchar(32),
	@i_terminal      varchar(32),
	@i_operacion     char(1),
	@i_codigo        varchar(5) = '',
	@i_nombre        varchar(500) = '',	
	@i_es_activo     varchar(1) = ''
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		select  IdTipoDocumento,
				NombreTipoDocumentos,				
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from TipoDocumento
		where EsActivo = 1
		and IdTipoDocumento > @i_codigo
		order by IdTipoDocumento asc
		
		return 0
	END
	
	
	
	IF @i_operacion = 'C'
	BEGIN
		select  IdTipoDocumento,
				NombreTipoDocumentos,				
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from TipoDocumento
		where EsActivo = 1
		and IdTipoDocumento = @i_codigo
		
		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		set @i_codigo = isnull((Select max(IdTipoDocumento) from TipoDocumento),0)+1
		
		INSERT INTO  TipoDocumento(IdTipoDocumento, NombreTipoDocumentos, EsActivo)
		VALUES (@i_codigo, @i_nombre, 1)
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE TipoDocumento
		set NombreTipoDocumentos = @i_nombre			
		WHERE IdTipoDocumento = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE TipoDocumento
		set EsActivo = 0			
		WHERE IdTipoDocumento = @i_codigo
		
		return 0
	END

END
GO


IF OBJECT_ID('dbo.sp_tipoDocumento') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_tipoDocumento') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_tipoDocumento >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_tipoDocumento >>>'
END
go

