USE sns_conta
GO

IF OBJECT_ID(N'dbo.sp_carga_comprobante_detalle') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_carga_comprobante_detalle
    IF OBJECT_ID(N'dbo.sp_carga_comprobante_detalle') IS NOT NULL
        PRINT N'<<< FAILED DROPPING PROCEDURE dbo.sp_carga_comprobante_detalle >>>'
    ELSE
        PRINT N'<<< DROPPED PROCEDURE dbo.sp_carga_comprobante_detalle >>>'
END
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_carga_comprobante_detalle](
    @i_user                             varchar(32),
	@i_terminal                         varchar(32),
	@i_operacion                        char(1),
	@Tabla							    [dbo].[BulkType] READONLY,
	@Recorrido							int	
)
AS
BEGIN
Declare @Delimitador char(1)

set @Delimitador = 'þ'


BEGIN TRY

    if @Recorrido = 0
	BEGIN
		TRUNCATE TABLE st_discount_his		
	END
	
	Insert into st_discount_his 
	(
	      dh_numusu  --1-- numeric(11,0) 
         ,dh_date    --2-- datetime      
         ,dh_total   --3-- numeric(12,4) 
		 
	)	
	Select  
    /*1 */convert(numeric(11,0) ,(Select case when len(ltrim(rtrim(dato)))<1 then '0' else dato end  from (Select ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as Seq,value as dato  from string_split(replace(demo.RegistroFila,',',@Delimitador), @Delimitador) ) tmp  where tmp.Seq= 1) ),
	/*2 */convert(date ,(Select case when len(ltrim(rtrim(dato)))<1 then '1900-01-01 00:00:00:000' else dato end  from (Select ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as Seq,value as dato  from string_split(replace(demo.RegistroFila,  ',',@Delimitador), @Delimitador) ) tmp  where tmp.Seq= 2),121),
	/*3 */convert(numeric(12,4) ,(Select case when len(ltrim(rtrim(dato)))<1 then '0' else dato end  from (Select ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as Seq,value as dato  from string_split(replace(demo.RegistroFila,',',@Delimitador), @Delimitador) ) tmp  where tmp.Seq= 3) )
	from @TablaMAX demo
	where upper(left(demo.RegistroFila,2))<> 'NU'

END TRY
BEGIN CATCH
		
	SELECT 
			@ErrorMessage	= ERROR_MESSAGE(),
			@ErrorSeverity	= ERROR_SEVERITY(),
			@ErrorState		= ERROR_STATE();

			--==============================================================================
				------------------------ llamado exception proceso ------------------------ 
			--LLamado SP que registra el log proceso
			--=============================================================================

			RAISERROR	(	@ErrorMessage,	-- Message text.
							@ErrorSeverity, -- Severity.
							@ErrorState		-- State.
						);

END CATCH

END
go

SET ANSI_NULLS OFF
go
SET QUOTED_IDENTIFIER OFF
go
IF OBJECT_ID(N'dbo.sp_carga_comprobante_detalle') IS NOT NULL
    PRINT N'<<< CREATED PROCEDURE dbo.sp_carga_comprobante_detalle >>>'
ELSE
    PRINT N'<<< FAILED CREATING PROCEDURE dbo.sp_carga_comprobante_detalle >>>'
go

GRANT EXECUTE ON  sp_carga_comprobante_detalle			to UserEC
