USE [sns_conta]
GO
/****** Object:  StoredProcedure [dbo].[sp_tipoComprobante]    Script Date: 6/9/2018 9:30:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.sp_tipoComprobante') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_tipoComprobante
    IF OBJECT_ID('dbo.sp_tipoComprobante') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_tipoComprobante >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_tipoComprobante >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <08/12/2020>
-- Description:	<SCRUD para Tipo Comprobante.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_tipoComprobante]
	@i_user          varchar(32),
	@i_terminal      varchar(32),
	@i_operacion     char(1),
	@i_codigo        varchar(5) = '',
	@i_nombre        varchar(500) = '',	
	@i_es_activo     varchar(1) = ''
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN
		select  IdTipoComprobante,
				NombreTipoComprobantes,				
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from TipoComprobante
		where EsActivo = 1
		and IdTipoComprobante > @i_codigo
		order by IdTipoComprobante asc
		
		return 0
	END
	
	
	
	IF @i_operacion = 'C'
	BEGIN
		select  IdTipoComprobante,
				NombreTipoComprobantes,				
				case EsActivo when 1 then 'S' else 'N' end as EsActivo
		from TipoComprobante
		where EsActivo = 1
		and IdTipoComprobante = @i_codigo
		
		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		INSERT INTO  TipoComprobante(IdTipoComprobante, NombreTipoComprobantes, EsActivo)
		VALUES (@i_codigo, @i_nombre, 1)
		
		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN
		
		UPDATE TipoComprobante
		set NombreTipoComprobantes = @i_nombre			
		WHERE IdTipoComprobante = @i_codigo
		
		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE TipoComprobante
		set EsActivo = 0			
		WHERE IdTipoComprobante = @i_codigo
		
		return 0
	END

END
GO


IF OBJECT_ID('dbo.sp_tipoComprobante') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_tipoComprobante') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_tipoComprobante >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_tipoComprobante >>>'
END
go

