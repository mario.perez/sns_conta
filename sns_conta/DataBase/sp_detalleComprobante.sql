USE [sns_conta]
GO
/****** Object:  StoredProcedure [dbo].[sp_detalleComprobante]    Script Date: 6/9/2018 9:30:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.sp_detalleComprobante') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_detalleComprobante
    IF OBJECT_ID('dbo.sp_detalleComprobante') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_detalleComprobante >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_detalleComprobante >>>'
END
go
 
-- =============================================
-- Author:		<Mario Leonardo Perez Tijerino>
-- Create date: <08/12/2020>
-- Description:	<SCRUD para Comprobantes.>
-- =============================================
CREATE PROCEDURE [dbo].[sp_detalleComprobante]
	@i_user          		varchar(32),
	@i_terminal      		varchar(32),
	@i_operacion     		char(1),
	@i_idComprobante   		numeric(18,0) = -1,
	@i_idDetalleComprobante numeric(18,0) = -1,
	@i_concepto        		varchar(1000) = '',	
	@i_debe   				numeric(16,2) = 0,	
	@i_haber   				numeric(16,2) = 0,	
	@i_idCuenta   			numeric(18,0) = 0,
 	@i_es_activo     		char(1) = '',
	@i_tabla				[dbo].[BulkType] READONLY,
	@i_recorrido            int = -1
AS
BEGIN
		
	IF @i_operacion = 'S'
	BEGIN

		SELECT [IdComprobante]
			,[IdDetalleComprobante]
			,[Concepto]
			,[Debe]
			,[Haber]
			,isnull([IdCuenta],0) as IdCuenta
			,case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM [dbo].[DetalleComprobante]
		WHERE EsActivo = 1
		AND IdComprobante = @i_idComprobante
		AND IdDetalleComprobante > @i_idDetalleComprobante
		ORDER BY IdDetalleComprobante asc

		return 0
	END
	
	IF @i_operacion = 'C'
	BEGIN
		SELECT [IdComprobante]
			,[IdDetalleComprobante]
			,[Concepto]
			,[Debe]
			,[Haber]
			,[IdCuenta]
			,case EsActivo when 1 then 'S' else 'N' end as EsActivo
		FROM [dbo].[DetalleComprobante]
		WHERE EsActivo = 1
		AND IdComprobante = @i_idComprobante
		AND IdDetalleComprobante = @i_idDetalleComprobante
		
		return 0
	END
	
	IF @i_operacion = 'I'
	BEGIN
		
		IF @i_idDetalleComprobante = -1 
		BEGIN
			set @i_idDetalleComprobante = 0
			
			Select @i_idDetalleComprobante = max(IdDetalleComprobante) from DetalleComprobante where IdComprobante = @i_idComprobante
			
			Set @i_idDetalleComprobante = isnull(@i_idDetalleComprobante,0) + 1
		END

		INSERT INTO [dbo].[DetalleComprobante]
			([IdComprobante],[IdDetalleComprobante],[Concepto]	--1
			,[Debe],[Haber],[IdCuenta]							--2
			,[EsActivo])										--3
		VALUES
				(@i_idComprobante,@i_idDetalleComprobante,@i_concepto	--1
				,@i_debe,@i_haber,@i_idCuenta							--2
				,1)														--3


		return 0
	END
	
	IF @i_operacion = 'U'
	BEGIN

		UPDATE [dbo].[DetalleComprobante]
		SET [Concepto] = @i_concepto
			,[Debe] = @i_debe
			,[Haber] = @i_haber
			,[IdCuenta] = @i_idCuenta
		WHERE [IdComprobante] = @i_idComprobante
		AND [IdDetalleComprobante] = @i_idDetalleComprobante

		return 0
	END
	
	
	IF @i_operacion = 'D'
	BEGIN
		
		UPDATE [dbo].[DetalleComprobante]
		set [EsActivo] = 0			
		WHERE [IdComprobante] = @i_idComprobante
		AND [IdDetalleComprobante] = @i_idDetalleComprobante

		return 0
	END

	IF @i_operacion = 'B'
	BEGIN	
		Declare @Delimitador char(1)
		set @Delimitador = 'þ'

		BEGIN TRY	

			Begin Transaction tran_bulk	
			if @i_recorrido = 0
			BEGIN
				DELETE FROM DetalleComprobante 
				WHERE IdComprobante = @i_idComprobante 
				and IdDetalleComprobante>=0
			END

			INSERT INTO [dbo].[DetalleComprobante]
				([IdComprobante] --1
				,[IdDetalleComprobante]--2
				,[Concepto]--3
				,[Debe]--4
				,[Haber]--5
				,[IdCuenta]--6
				,[EsActivo])--7     
			Select  
			/*1 */convert(numeric(18,0) ,(Select case when len(ltrim(rtrim(dato)))<1 then '0' else dato end  from (Select ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as Seq,value as dato  from string_split(replace(demo.Record,',',@Delimitador), @Delimitador) ) tmp  where tmp.Seq= 1) ),
			/*2 */convert(numeric(18,0) ,(Select case when len(ltrim(rtrim(dato)))<1 then '0' else dato end  from (Select ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as Seq,value as dato  from string_split(replace(demo.Record,',',@Delimitador), @Delimitador) ) tmp  where tmp.Seq= 2) ),
			/*3 */convert(varchar(1000) ,(Select case when len(ltrim(rtrim(dato)))<1 then '' else dato end  from (Select ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as Seq,value as dato  from string_split(replace(demo.Record,',',@Delimitador), @Delimitador) ) tmp  where tmp.Seq= 3) ),
			/*4 */convert(numeric(16,2) ,(Select case when len(ltrim(rtrim(dato)))<1 then '0' else dato end  from (Select ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as Seq,value as dato  from string_split(replace(demo.Record,',',@Delimitador), @Delimitador) ) tmp  where tmp.Seq= 4) ),	
			/*5 */convert(numeric(16,2) ,(Select case when len(ltrim(rtrim(dato)))<1 then '0' else dato end  from (Select ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as Seq,value as dato  from string_split(replace(demo.Record,',',@Delimitador), @Delimitador) ) tmp  where tmp.Seq= 5) ),	
			/*6 */convert(numeric(18,0) ,(Select case when len(ltrim(rtrim(dato)))<1 then '0' else dato end  from (Select ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as Seq,value as dato  from string_split(replace(demo.Record,',',@Delimitador), @Delimitador) ) tmp  where tmp.Seq= 6) ),	
			/*7 */1
			from @i_tabla demo


			Commit Transaction tran_bulk;
		END TRY
		BEGIN CATCH
			Rollback Transaction tran_bulk;
			THROW;
		END CATCH

		return 0
	END

END
GO


IF OBJECT_ID('dbo.sp_detalleComprobante') IS NOT NULL
BEGIN
    IF OBJECT_ID('dbo.sp_detalleComprobante') IS NOT NULL
        PRINT '<<< CREATED PROCEDURE dbo.sp_detalleComprobante >>>'
    ELSE
        PRINT '<<< FAILED CREATING PROCEDURE dbo.sp_detalleComprobante >>>'
END
go

