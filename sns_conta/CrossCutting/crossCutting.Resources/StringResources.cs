﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crossCutting.Resources
{
    public sealed class StringResources
    {
        private static StringResources _instance = null;
        private readonly string _name = "Nombre"; 
        private readonly string _id = "Código"; 
        private readonly string _emptyId = "El código no puede estar vacío"; 
        private readonly string _status = "Estado"; 
        private readonly string _emptyName = "El nombre no puede estar vacío"; 
        private readonly string _notActiveStatus = "No se puede crear si el estado no es activo"; 
        private readonly string _dbFailure = "No se guardó la información, revisé la conexión a la base de datos";
        private readonly string _emptyValue = "El valor no puede estar vacío.";
        private readonly string _value = "Valor";
        private readonly string _emptyDescription = "La descripción no puede estar vacía.";
        private readonly string _description = "Descripción";
        private readonly string _unitsPerPackage = "Unidades por paquete";
        private readonly string _unitsPerPackageValue = "Las unidades por paquete no pueden ser cero.";
        private readonly string _currencyIso = "Moneda";
        private readonly string _currencyIsoValue = "El valor de la moneda no puede estar vacío.";
        private readonly string _currencyNumber = "El número de la moneda no puede estar vacío";
        private readonly string _currencyDecimal = "El decimal de la moneda no puede estar vacío";
        private readonly string _factor = "Factor";
        private readonly string _cashbox = "Caja";
        private readonly string _cashboxValue = "La informacion de Caja no puede estar vacía";
        private readonly string _userNameValue = "El nombre de usuario no puede estar vacío";
        private readonly string _userName = "Usuario";
        private readonly string _finalDate = "Fecha final";
        private readonly string _InitialDateVsFinalDate = "La fecha final no puede ser menor a la fecha inicial.";
        private readonly string _createDate = "La fecha de ingreso no puede ser mayor a la fecha actual.";
        private readonly string _date = "Fecha";
        private readonly string _warehouse = "Bodega";
        private readonly string _warehouseValue = "El codigo de bodega esta vacía.";





        public static StringResources Instance
        {
            get
            {
                lock (_instance)
                {
                    if (_instance == null)
                        _instance = new StringResources();

                    return _instance;
                }
            }
        }

        public string Name => _name;

        public string Id => _id;

        public string EmptyId => _emptyId;

        public string Status => _status;

        public string EmptyName => _emptyName;

        public string NotActiveStatus => _notActiveStatus;

        public string DbFailure => _dbFailure;

        public string EmptyValue => _emptyValue;

        public string Value => _value;

        public string EmptyDescription => _emptyDescription;

        public string Description => _description;

        public string UnitsPerPackage => _unitsPerPackage;

        public string UnitsPerPackageValue => _unitsPerPackageValue;

        public string CurrencyIsoValue => _currencyIsoValue;

        public string CurrencyIso => _currencyIso;

        public string CurrencyNumber => _currencyNumber;

        public string CurrencyDecimal => _currencyDecimal;

        public string Factor => _factor;

        public string Cashbox => _cashbox;

        public string CashboxValue => _cashboxValue;

        public string UserNameValue => _userNameValue;

        public string UserName => _userName;

        public string FinalDate => _finalDate;

        public string InitialDateVsFinalDate => _InitialDateVsFinalDate;

        public string CreateDate => _createDate;

        public string Date => _date;

        public string Warehouse => _warehouse;

        public string WarehouseValue => _warehouseValue;

        private StringResources()
        {
        }

        
     
    }
}
