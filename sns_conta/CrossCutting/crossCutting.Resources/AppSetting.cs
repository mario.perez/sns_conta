﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;


namespace crossCutting.Resources
{
    public sealed class AppSetting
    {
        private static readonly AppSetting _instance = new AppSetting();

        #region private declarations...

        private string _stringConnection = string.Empty;
        private string _databaseName = string.Empty;
        #endregion

        #region public scopes
        public string StringConnection{ get { return _stringConnection; } }
        public string DatabaseName { get { return _databaseName; } }
        #endregion


        private AppSetting()
        {
            try
            {
                _stringConnection = ConfigurationSettings.AppSettings["ConnectionString"];
                _databaseName = ConfigurationSettings.AppSettings["DatabaseName"];
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + (ex.InnerException != null ? ex.InnerException.Message + Environment.NewLine + ex.InnerException.StackTrace : ""));
            }
        }

        public static AppSetting Instance
        {
            get
            {
                return _instance;
            }
        }

    }

}
