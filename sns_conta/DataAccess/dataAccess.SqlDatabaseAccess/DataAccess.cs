﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using crossCutting.Resources;

namespace dataAccess.SqlDatabaseAccess
{
    public class DataAccess
    {
        private SqlConnection cnn = null;
        private List<SqlParameter> parameters = null;

        public DataAccess()
        {
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                cnn = new SqlConnection(AppSetting.Instance.StringConnection);
                cnn.InfoMessage += new SqlInfoMessageEventHandler(PrintInfoHandler);
            }
            catch (Exception ex)
            {
                throw new Exception("Cadena:" + AppSetting.Instance.StringConnection + Environment.NewLine + " ex: " + ex.Message + Environment.NewLine + "StackTrace: " + ex.StackTrace + Environment.NewLine + "InnerException: " + ex.InnerException.StackTrace);
            }
        }

        private void PrintInfoHandler(object sender, SqlInfoMessageEventArgs e)
        {
            foreach (SqlError error in e.Errors)
            {
                if (error.Number != 5701)
                    throw new Exception("[ServerName:] " + error.Server + Environment.NewLine +
                        " [ProcName:] " + error.Procedure + Environment.NewLine +
                        " [Line:] " + error.LineNumber.ToString() + Environment.NewLine +
                        " [Number:] " + error.Number.ToString() + Environment.NewLine +
                        " [Message:] " + error.Message);
            }
        }

        private void ErrorHandler(SqlException ex)
        {
            foreach (SqlError error in ex.Errors)
            {
                throw new Exception("[ServerName:] " + error.Server + Environment.NewLine +
                        " [ProcName:] " + error.Procedure + Environment.NewLine +
                        " [Line:] " + error.LineNumber.ToString() + Environment.NewLine +
                        " [Number:] " + error.Number.ToString() + Environment.NewLine +
                        " [Message:] " + error.Message);
            }
        }

        private void ErrorHandler(Exception ex)
        {
            throw new Exception("[ServerName:] " + "Sistema" + Environment.NewLine +
                " [ProcName:] " + "DataFactory" + Environment.NewLine +
                " [Message:] " + ex.Message + Environment.NewLine + ex.StackTrace);
        }

        public void AdParameter(string paramName, DBTypeEnum paramType, Object paramValue)
        {
            if (parameters == null)
                parameters = new List<SqlParameter>();

            this.AdParameter(paramName, paramType, ParameterDirection.Input, paramValue);
        }

        public void AdParameterOutput(string paramName, DBTypeEnum paramType, Object paramValue)
        {
            if (parameters == null)
                parameters = new List<SqlParameter>();

            this.AdParameter(paramName, paramType, ParameterDirection.Output, paramValue);
        }

        public void Open()
        {
            cnn.Open();
        }

        public void Close()
        {
            cnn.Close();
        }

        public void AdParameter(string paramName, DBTypeEnum paramType, ParameterDirection paramDirection, Object paramValue)
        {
            if (parameters == null)
                parameters = new List<SqlParameter>();

            SqlDbType internalType = (SqlDbType)DBTypeHelper.ConvertDBType(paramType);
            SqlParameter internalParam = new SqlParameter(paramName, internalType);
            internalParam.Direction = paramDirection;
            internalParam.Value = paramValue;
            this.parameters.Add(internalParam);
        }

        public bool ExecProcedure(string spDBName, string spName, out DataSet spOutData)
        {
            return this.ExecQuery(spDBName, spName, CommandType.StoredProcedure, out spOutData);
        }

        public bool ExecProcedure(string spDBName, string spName, out DataSet spOutData,ref  Dictionary<string,string> outputParameters)
        {
            return this.ExecQuery(spDBName, spName, CommandType.StoredProcedure, out spOutData, ref outputParameters);
        }

        public bool ExecProcedure(string spDBName, string spName, out Int32 spOutData)
        {
            return this.ExecNonQuery(spDBName, spName, CommandType.StoredProcedure, out spOutData);
        }

        public bool ExecProcedure(string spDBName, string spName, bool silentMode, out Int32 spOutData)
        {
            return this.ExecNonQuery(spDBName, spName, CommandType.StoredProcedure, silentMode, out spOutData);
        }

        public bool ExecProcedure(string spDBName, string spName, out object spOutData)
        {
            return this.ExecuteScalar(spDBName, spName, CommandType.StoredProcedure, out spOutData);
        }

        public bool ExecQuery(string queryDBName, string queryString, out DataSet queryOutData)
        {
            return this.ExecQuery(queryDBName, queryString, CommandType.Text, out queryOutData);
        }

        public bool ExecQuery(string queryDBName, string queryString, out Int32 queryOutData)
        {
            return this.ExecNonQuery(queryDBName, queryString, CommandType.Text, out queryOutData);
        }

        public bool ExecQuery(string queryDBName, string queryString, out object queryOutData)
        {
            return this.ExecuteScalar(queryDBName, queryString, CommandType.Text, out queryOutData);
        }

        private bool ExecQuery(string queryDBName, string queryString, CommandType queryType, out DataSet queryOutData, ref Dictionary<string,string> outputParameters)
        {
            bool result = false;
            SqlCommand cmd;
            SqlDataAdapter adap;
            queryOutData = new DataSet();

            try
            {
                if (cnn.State != ConnectionState.Open)
                    cnn.Open();
                cnn.ChangeDatabase(queryDBName);
                cmd = new SqlCommand(queryString, cnn);
                cmd.CommandType = queryType;
                cmd.CommandTimeout = cnn.ConnectionTimeout;
                if (queryType == CommandType.StoredProcedure)
                {
                    AdParameter("@i_user", DBTypeEnum.VarChar, System.Threading.Thread.CurrentPrincipal.Identity.Name);
                    AdParameter("@i_terminal", DBTypeEnum.VarChar, System.Environment.MachineName);
                }
                if (parameters != null)
                {
                    foreach (SqlParameter param in parameters)
                    {
                        cmd.Parameters.Add(param);
                    }
                }
                adap = new SqlDataAdapter(cmd);
                adap.Fill(queryOutData);

                List<string> outsList = new List<string>();

                foreach (var param in outputParameters)
                    outsList.Add(param.Key);

                foreach(var param in outsList)
                    outputParameters[param] =  cmd.Parameters[param].Value.ToString();
                

                cnn.Close();
                if (cmd != null)
                    cmd.Dispose();
                cmd = null;
                if (adap != null)
                    adap.Dispose();
                adap = null;
                if (parameters != null)
                    parameters.Clear();
                parameters = null;

                result = true;
            }
            catch (SqlException ex)
            {
                this.ErrorHandler(ex);
                queryOutData = null;
            }
            catch (Exception ex)
            {
                this.ErrorHandler(ex);
                queryOutData = null;
            }

            return result;
        }

        private bool ExecQuery(string queryDBName, string queryString, CommandType queryType, out DataSet queryOutData)
        {
            bool result = false;
            SqlCommand cmd;
            SqlDataAdapter adap;
            queryOutData = new DataSet();

            try
            {
                if (cnn.State != ConnectionState.Open)
                    cnn.Open();
                cnn.ChangeDatabase(queryDBName);
                cmd = new SqlCommand(queryString, cnn);
                cmd.CommandType = queryType;
                cmd.CommandTimeout = cnn.ConnectionTimeout;
                if (queryType == CommandType.StoredProcedure)
                {
                    AdParameter("@i_user", DBTypeEnum.VarChar, System.Threading.Thread.CurrentPrincipal.Identity.Name);
                    AdParameter("@i_terminal", DBTypeEnum.VarChar, System.Environment.MachineName);
                }
                if (parameters != null)
                {
                    foreach (SqlParameter param in parameters)
                    {
                        cmd.Parameters.Add(param);
                    }
                }
                adap = new SqlDataAdapter(cmd);
                adap.Fill(queryOutData);

                cnn.Close();
                if (cmd != null)
                    cmd.Dispose();
                cmd = null;
                if (adap != null)
                    adap.Dispose();
                adap = null;
                if (parameters != null)
                    parameters.Clear();
                parameters = null;

                result = true;
            }
            catch (SqlException ex)
            {
                this.ErrorHandler(ex);
                queryOutData = null;
            }
            catch (Exception ex)
            {
                this.ErrorHandler(ex);
                queryOutData = null;
            }

            return result;
        }

        private bool ExecNonQuery(string nonQueryDBName, string nonQueryString, CommandType nonQueryType, out Int32 nonQueryOutData)
        {
            return this.ExecNonQuery(nonQueryDBName, nonQueryString, nonQueryType, false, out nonQueryOutData);
        }

        private bool ExecNonQuery(string nonQueryDBName, string nonQueryString, CommandType nonQueryType, bool silentMode, out Int32 nonQueryOutData)
        {
            bool result = false;
            SqlCommand cmd = null;

            if (silentMode)
            {
                try
                {
                    cnn.InfoMessage -= new SqlInfoMessageEventHandler(PrintInfoHandler);
                }
                catch (Exception)
                {
                }
            }

            try
            {
                if (cnn.State != ConnectionState.Open)
                    cnn.Open();
                cnn.ChangeDatabase(nonQueryDBName);
                cmd = new SqlCommand(nonQueryString, cnn);
                cmd.CommandType = nonQueryType;
                cmd.CommandTimeout = cnn.ConnectionTimeout;
                if (nonQueryType == CommandType.StoredProcedure)
                {
                    AdParameter("@i_user", DBTypeEnum.VarChar, System.Threading.Thread.CurrentPrincipal.Identity.Name);
                    AdParameter("@i_terminal", DBTypeEnum.VarChar, System.Environment.MachineName);
                }
                if (parameters != null)
                {
                    foreach (SqlParameter param in parameters)
                    {
                        cmd.Parameters.Add(param);
                    }
                }
                nonQueryOutData = cmd.ExecuteNonQuery();

                result = true;
            }
            catch (SqlException ex)
            {
                if (!silentMode)
                    this.ErrorHandler(ex);
                nonQueryOutData = -1;
            }
            catch (Exception ex)
            {
                if (!silentMode)
                    this.ErrorHandler(ex);
                nonQueryOutData = -1;
            }
            finally
            {
                cnn.Close();
                if (cmd != null)
                    cmd.Dispose();
                cmd = null;
                if (parameters != null)
                    parameters.Clear();
                parameters = null;
            }

            if (silentMode)
            {
                try
                {
                    cnn.InfoMessage += new SqlInfoMessageEventHandler(PrintInfoHandler);
                }
                catch (Exception)
                {
                }
                result = true;
            }

            return result;
        }

        private bool ExecuteScalar(string scalarQueryDBName, string scalarQueryString, CommandType scalarQueryType, out object scalarQueryOutData)
        {
            bool result = false;
            SqlCommand cmd;
            try
            {
                if (cnn.State != ConnectionState.Open)
                    cnn.Open();
                cnn.ChangeDatabase(scalarQueryDBName);
                cmd = new SqlCommand(scalarQueryString, cnn);
                cmd.CommandType = scalarQueryType;
                cmd.CommandTimeout = cnn.ConnectionTimeout;
                if (scalarQueryType == CommandType.StoredProcedure)
                {
                    AdParameter("@i_user", DBTypeEnum.VarChar, System.Threading.Thread.CurrentPrincipal.Identity.Name);
                    AdParameter("@i_terminal", DBTypeEnum.VarChar, System.Environment.MachineName);
                }
                if (parameters != null)
                {
                    foreach (SqlParameter param in parameters)
                    {
                        cmd.Parameters.Add(param);
                    }
                }
                scalarQueryOutData = cmd.ExecuteScalar();

                cnn.Close();
                if (cmd != null)
                    cmd.Dispose();
                cmd = null;
                if (parameters != null)
                    parameters.Clear();
                parameters = null;

                result = true;
            }
            catch (SqlException ex)
            {
                this.ErrorHandler(ex);
                scalarQueryOutData = null;
            }
            catch (Exception ex)
            {
                this.ErrorHandler(ex);
                scalarQueryOutData = null;
            }

            return result;
        }

        #region Miembros de IDisposable

        public void Dispose()
        {
            try
            {
                if (this.cnn != null)
                    this.cnn.Close();
            }
            catch
            {
            }

            try
            {
                if (this.cnn != null)
                    this.cnn.Dispose();
            }
            catch
            {
            }

            this.cnn = null;
        }

        #endregion
    }

}
