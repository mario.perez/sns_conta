﻿namespace dataAccess.SqlDatabaseAccess
{
    public enum DBTypeEnum
    {
        BigDateTime = 0,
        BigInt = 1,
        Binary = 2,
        Bit = 3,
        Char = 4,
        Date = 5,
        DateTime = 6,
        Decimal = 7,
        Double = 8,
        Image = 9,
        Integer = 10,
        Money = 11,
        NChar = 12,
        Numeric = 13,
        NVarChar = 14,
        Real = 15,
        SmallDateTime = 16,
        SmallInt = 17,
        SmallMoney = 18,
        Text = 19,
        Time = 20,
        TimeStamp = 21,
        TinyInt = 22,
        UniChar = 23,
        Unitext = 24,
        UniVarChar = 25,
        UnsignedBigInt = 26,
        UnsignedInt = 27,
        UnsignedSmallInt = 28,
        Unsupported = 29,
        VarBinary = 30,
        VarChar = 31
    }
}
