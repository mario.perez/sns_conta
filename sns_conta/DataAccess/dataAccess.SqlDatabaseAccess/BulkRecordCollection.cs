﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dataAccess.SqlDatabaseAccess
{
    public class BulkRecordCollection : List<BulkRecord>, IEnumerable<SqlDataRecord>
    {
        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            var sqlRow = new SqlDataRecord(
                  new SqlMetaData("Record", SqlDbType.VarChar, SqlMetaData.Max)
                  );

            foreach (BulkRecord cust in this)
            {
                sqlRow.SetString(0, cust.RecordData);
                yield return sqlRow;
            }
        }
    }
}
