﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using crossCutting.Resources;
using dataAccess.SqlDatabaseAccess;

namespace dataAccess.BaseManagement
{
    public class CustomCatalogManager<T>
    {
        protected string _procedureName;
        protected Dictionary<string, string> _parameters;

        public Dictionary<string,string> OutputParameters { get => _parameters; set => _parameters = value; }

        private static DataRowCollection GetTable(string procedureName, string code)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "S");
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, code);

            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response.Rows;
        }

        private static DataRowCollection GetRow(string procedureName, string code)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            DataTable response;
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "C");
            dataAccess.AdParameter("@i_codigo", DBTypeEnum.VarChar, code);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            if (!result)
            {
                response = null;
            }
            else
            {
                if ((dataSet != null) && (dataSet.Tables.Count > 0) && (dataSet.Tables[0].Rows.Count > 0))
                {
                    response = dataSet.Tables[0];
                }
                else
                {
                    response = new DataTable();
                }
            }
            return response.Rows;
        }

        protected List<T> GetList(string procedureName, string code)
        {
            var response = GetTable(procedureName,code);
            List<T> catalogList = new List<T>();
            if(response == null)
            {
                return catalogList;
            }
            foreach(DataRow row in response)
            {
                T catalog = getCatalog(row);
                catalogList.Add(catalog);
            }
            return catalogList;
        }

        protected virtual T getCatalog(DataRow row)
        {
            throw new NotImplementedException();
        }

        protected  T GetData(string procedureName, string code)
        {
            var response = GetRow(procedureName, code);
            T result;
            result = getNewCatalog();
            foreach(DataRow row in response)
            {
                result = getCatalog(row);
            }
            return result;
        }

        protected virtual  T getNewCatalog()
        {
            throw new NotImplementedException();
        }

        protected bool InsertData(string procedureName, T data)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            Dictionary<string, string> parameters;
            
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "I");            
            setCatalog(dataAccess,data);
            setOutputs(out parameters);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet, ref parameters);
            dataAccess.Dispose();
            dataAccess = null;
            _parameters = new Dictionary<string, string>();
            foreach(var param in parameters)
            {
                _parameters.Add(param.Key, param.Value);
            }
            
            return result;
        }

        protected virtual void setOutputs(out Dictionary<string, string> parameters)
        {
            parameters = new Dictionary<string, string>();            
        }

        protected virtual void setCatalog(DataAccess dataAccess, T data)
        {
            throw new NotImplementedException();
        }

        protected virtual string getBulkRecord(T t)
        {
            throw new NotImplementedException();
        }

        protected bool DeleteData(string procedureName, T data)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "D");
            setCatalog(dataAccess, data);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }


        protected bool UpdateData(string procedureName, T data)
        {
            DataAccess dataAccess = new DataAccess();
            DataSet dataSet;
            
            bool result = false;
            dataAccess.AdParameter("@i_operacion", DBTypeEnum.VarChar, "U");
            setCatalog(dataAccess, data);
            result = dataAccess.ExecProcedure(crossCutting.Resources.AppSetting.Instance.DatabaseName, procedureName, out dataSet);
            dataAccess.Dispose();
            dataAccess = null;
            return result;
        }

        public bool LoadBulk(T[] details)
        {
            SqlConnection connection = new SqlConnection(AppSetting.Instance.StringConnection);
            var records = new List<BulkRecord>();
            for (var i = 0; i < details.Length; i++)
            {
                records.Add(new BulkRecord() { RecordData = getBulkRecord(details[i]) });
            }

            connection.Open();
            using (SqlCommand command = new SqlCommand(_procedureName, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                BulkRecordCollection custColl = new BulkRecordCollection();
                custColl.AddRange(records);

                SqlParameter param = command.Parameters.AddWithValue("@i_tabla", custColl);
                command.Parameters.AddWithValue("@i_recorrido", 0);
                command.Parameters.AddWithValue("@i_user", "");
                command.Parameters.AddWithValue("@i_terminal", "");
                command.Parameters.AddWithValue("@i_operacion", "B");

                param.SqlDbType = SqlDbType.Structured;
                param.TypeName = "dbo.BulkType";
                param.Direction = ParameterDirection.Input;
                command.ExecuteNonQuery();

            }

            connection.Close();
            return true;
        }


        public bool Add(T data)
        {
            var result = InsertData(_procedureName, data);
            return result;
        }

        public List<T> GetAll(string code)
        {
            var result = GetList(_procedureName,code);
            return result;
        }

        public T GetOne(string id)
        {
            var result = GetData(_procedureName, id);
            return result;
        }

        public bool Remove(T data)
        {
            var result = DeleteData(_procedureName, data);
            return result;
        }

        public bool Update(T data)
        {
            var result = UpdateData(_procedureName, data);
            return result;
        }

    }
}
