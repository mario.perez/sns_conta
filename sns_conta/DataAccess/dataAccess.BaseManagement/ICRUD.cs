﻿using System.Collections.Generic;


namespace dataAccess.BaseManagement
{
    public interface ICRUD<T>
    {
        bool Add(T data);

        bool Remove(T data);

        List<T> GetAll(string code);

        T GetOne(string id);

        bool Update(T data);
        
    }

    
}
